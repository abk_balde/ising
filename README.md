# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Metropolis Monte Carlo Simulation of the Ising Model in 2D and 3D using Thrust, Random123, y openGL.

* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Simulaciones Interactivas, via openGL (ejemplos):

2D simulation, square DIMxDIM, sin desorden, con random-bond (RBDISORDER) o random-field disorder (RBDISORDER)
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DRBDISORDER=0.1
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DRFDISORDER=0.1

3D simulation, square DIMxDIMxDIMZ, sin desorden, con random-bond (RBDISORDER) o random-field disorder (RBDISORDER)
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DDIMZ=32
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DDIMZ=32 -DRBDISORDER=0.1
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DDIMZ=32 -DRFDISORDER=0.1

Simulaciones No interactivas (ejemplos):

nvcc -arch=sm_20 -o ising ising.cu -DDIM=512 -DRBDISORDER=0.1
nvcc -arch=sm_20 -o ising ising.cu -DDIM=512 -DDIMZ=32 -DRFDISORDER=0.1

* Configuration

En modo interactivo las dimensiones son los macros DIM, y DIMZ.
En modo no-interactivo, L==DIM, Lz==DIMZ, se los puede pasar por linea de comandos. 
Por ejemplo, 2d y 3d

./ising L temp field trun seed

./ising L Lz temp field trun seed


* Dependencies

Cuda-toolkit + Random123 

Para la interactiva, todo lo necesario para hacer andar simpleGL del cuda-samples.

* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
