/*
Compilacion (en susa): 2d o 3d
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512
nvcc -arch=sm_20 -o pbo_opengl_template pbo_opengl_template.cu -lglut -lGLU -lGL -lcufft -DDIM=512 -DDIMZ=32

Este file contiene codigo exclusivo a opengl. 
Para conectar con un sistema simulado, se agregan 
misistema.h => clase del sistema simulado. TOTALMENTE INDEPENDIENTE de opengl
interface.h => funciones utiles para mapear el sistema simulado al buffer de opengl. DEPENDIENTE DE OPENGL.
*/

#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <GL/gl.h> 
#include <GL/glut.h> 
#include <cuda_gl_interop.h> 
#include <GL/glext.h> 
#include <GL/glx.h> 

#define REFRESH_DELAY     10 //ms

#define GET_PROC_ADDRESS( str ) glXGetProcAddress( (const GLubyte *)str ) 

static void HandleError( cudaError_t err, const char *file,  int line ) { 
    if (err != cudaSuccess) { 
            printf( "%s in %s at line %d\n", cudaGetErrorString( err ),  file, line ); 
            exit( EXIT_FAILURE ); 
    } 
} 
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ )) 


PFNGLBINDBUFFERARBPROC    glBindBuffer     = NULL; 
PFNGLDELETEBUFFERSARBPROC glDeleteBuffers  = NULL; 
PFNGLGENBUFFERSARBPROC    glGenBuffers     = NULL; 
PFNGLBUFFERDATAARBPROC    glBufferData     = NULL; 


GLuint  bufferObj; 
cudaGraphicsResource *resource; 

////////////////////////////////////////////////////////

// define la interface entre opengl y mi sistema //////
#include "util_GPU.h"
#include "interface.h"                                //

static void draw_func( void ) { 
  rescale();
  change_pixels();

  glDrawPixels( DIM, DIM, GL_RGBA, GL_UNSIGNED_BYTE, 0 ); 

  glutSwapBuffers(); 

}

static void key_func( unsigned char key, int x, int y ) { 
  switch (key) { 
    case 27: 
        HANDLE_ERROR( cudaGraphicsUnregisterResource( resource ) ); 
        glBindBuffer( GL_PIXEL_UNPACK_BUFFER_ARB, 0 ); 
        glDeleteBuffers( 1, &bufferObj ); 
        exit(0); 
        break;
    case 32: // space
        paso=0; 
        break;
    case 8: // backspace
        Sptr->reinicializar(signo);
        break;
    case 9: // tab 
        rescale();
        break;
    case '0':
        paso=0;
        break;
    case '1':
	//std::cout << "key=" << key << std::endl;
        paso=1;
        break;
    case '2':
        paso=2;
        break;
    case '3':
        paso=4;
        break;
    case '4':
        paso=8;
        break;
    case '5':
        paso=16;
        break;
    case '6':
        paso=32;
        break;
    case 'C':
        //Sptr->addcircle(DIM*0.5, DIM*0.25, DIM*0.12,1);
        //Sptr->addcircle(DIM*0.5, DIM*0.75, DIM*0.12,1);
        break;
    case 'c':
        //Sptr->addcircle(DIM*0.5, DIM*0.5, DIM*0.12,1);
        break;
    case 'u':
        //Sptr->addtext(0,0,1,512*512);
        break;
    case 'y':
        //Sptr->halfandhalf();
        break;
    case 'w':
        //Sptr->detect_wall(outwall);
        break;
    case 'x':
        signo*=-1;
        break;    
    case 'h':
        hext+=HINC;
	Sptr->set_field(hext);        
	std::cout << "hext=" << hext << std::endl;
	break;    
    case 'H':
        hext-=HINC;
	Sptr->set_field(hext);        
	std::cout << "hext=" << hext << std::endl;
        break;    
    case 'T':
        if(temp-TINC>0) temp-=TINC; 
	Sptr->set_temp(temp);        
	std::cout << "temp=" << temp << std::endl;
        break;    
    case 't':
        temp+=TINC; 
	Sptr->set_temp(temp);        
	std::cout << "temp=" << temp << std::endl;
        break;    
    case 'j':
	std::cout << "hext=" << hext << std::endl;
        std::cin >> hext;
	Sptr->set_field(hext);        
	std::cout << "new hext=" << hext << std::endl;
        break;    
    case 'J':
	std::cout << "temp=" << temp << std::endl;
        std::cin >> temp; 
	if(temp>0) Sptr->set_temp(temp);        
	std::cout << "new temp=" << temp << std::endl;
        break;    
    case 'z':
	#ifdef DIMZ
	planoz++; planoz=planoz%DIMZ;
	#endif
	std::cout << "planoz=" << planoz << std::endl;
        break;    
    case 'Z':
	#ifdef DIMZ
	planoz--; if(planoz<0) planoz+=DIMZ; planoz=planoz%DIMZ;
	#endif
	std::cout << "planoz=" << planoz << std::endl;
        break;    
    default:
        break;
  } 
} 

////////////////////////////////////////////////////////////////////////////////
//! Mouse event handlers
////////////////////////////////////////////////////////////////////////////////
// mouse controls
int mouse_old_x, mouse_old_y;
int mouse_buttons = 0;
float rotate_x = 0.0, rotate_y = 0.0;
float translate_z = -3.0;

void mouse(int button, int state, int x, int y)
{
    if (state == GLUT_DOWN)
    {
        mouse_buttons |= 1<<button;
    }
    else if (state == GLUT_UP)
    {
        mouse_buttons = 0;
    }

    mouse_old_x = x;
    mouse_old_y = y;

    sqx=(x>0 && x<DIM)?(x):(sqx); sqy=(y>0 && y<DIM)?(y):(sqy);
    std::cout << sqx << " (clicks) " << sqy << " " << signo << std::endl;
    //Sptr->addsquare(sqx,DIM-sqy, 50);
    //draw_func();	
}

void motion(int x, int y)
{
    float dx, dy;
    dx = (float)(x - mouse_old_x);
    dy = (float)(y - mouse_old_y);

    if (mouse_buttons & 1)
    {
        rotate_x += dy * 0.2f;
        rotate_y += dx * 0.2f;
    }
    else if (mouse_buttons & 4)
    {
        translate_z += dy * 0.01f;
    }

    mouse_old_x = x;
    mouse_old_y = y;

    sqx=(x>0 && x<DIM)?(x):(sqx); sqy=(y>0 && y<DIM)?(y):(sqy);

    std::cout << sqx << " (motion) " << sqy << std::endl;
    Sptr->addsquare(sqx,DIM-sqy,planoz, 10, signo);
}




void timerEvent(int value)
{
    glutPostRedisplay();
    glutTimerFunc(REFRESH_DELAY, timerEvent,0);
}

int main(int argc, char *argv[]) { 

  //int numero;
  //std::cin >> numero;
  //test(numero,DIM);
  //exit(1);

  ////// declarar/inicializar sistema //////
  #ifndef DIMZ 	
  ising_system S(DIM); 
  #else   
  ising_system S(DIM, DIMZ); 
  #endif	

  Sptr=&S;
  inicializar_variables_globales_sistema();
  //////////////////////////////////////////

  cudaGLSetGLDevice( 0 ); 

  glutInit( &argc, argv ); 
  glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA ); 
  glutInitWindowSize( DIM, DIM ); 

  char msg[50];  
  #ifndef DIMZ	
  sprintf(msg,"Modelo de Ising 2D en GPU, %d espines (ABK)", DIM*DIM);	
  #else
  sprintf(msg,"Modelo de Ising 3D en GPU, %d espines (ABK)", DIM*DIM*DIMZ);	  
  #endif	
  glutCreateWindow(msg); 

  glBindBuffer    = (PFNGLBINDBUFFERARBPROC)GET_PROC_ADDRESS("glBindBuffer"); 
  glDeleteBuffers = (PFNGLDELETEBUFFERSARBPROC)GET_PROC_ADDRESS("glDeleteBuffers"); 
  glGenBuffers    = (PFNGLGENBUFFERSARBPROC)GET_PROC_ADDRESS("glGenBuffers"); 
  glBufferData    = (PFNGLBUFFERDATAARBPROC)GET_PROC_ADDRESS("glBufferData"); 

  glGenBuffers( 1, &bufferObj ); 
  glBindBuffer( GL_PIXEL_UNPACK_BUFFER_ARB, bufferObj ); 
  glBufferData( GL_PIXEL_UNPACK_BUFFER_ARB, DIM * DIM * 4, NULL, GL_DYNAMIC_DRAW_ARB ); 


  cudaGraphicsGLRegisterBuffer( &resource, bufferObj, cudaGraphicsMapFlagsNone ); 

// set up GLUT and kick off main loop 
  glutKeyboardFunc( key_func ); 
  glutDisplayFunc( draw_func ); 
  glutMouseFunc(mouse);
  glutMotionFunc(motion);

  glutTimerFunc(REFRESH_DELAY, timerEvent,0);
  glutMainLoop(); 
}
