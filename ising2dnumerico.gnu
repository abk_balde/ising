system("cudarun ./pbo_opengl_template &"); 

set ylabel 'M'; 
set xlabel 'T'; 
set arrow 1 from 2.27,-1 to 2.27,1 nohead lw 3; 

#set multi lay 1,2

do for[i=0:100000]{ 
	plot [][-1:1] '< tail -n 100000 magnetizacion.dat' u 3:2:1 lc pal t '',0, \
	(1.-1./sinh(2.0/x)**4)**(1./8.),-(1.-1./sinh(2.0/x)**4)**(1./8.); 

	pause .2;
}

#unset multi
