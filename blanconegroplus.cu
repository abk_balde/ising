#include<iostream>
#include<cstdlib>
#include<thrust/host_vector.h>
#include<thrust/device_vector.h>
#include <thrust/tuple.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>
#include<thrust/iterator/counting_iterator.h>

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG

/*
bool esNegro(int n, int L)
{
	return ((n%L+int(n/L))%2==0);
}

thrust::tuple<int,int,int,int> vecinos2d(int n, int L)
{
	int nx=n%L;
       	int ny=int(n/L);
	return thrust::make_tuple(
		(nx-1+L)%L  + ny*L,  // derecha
		(nx+1+L)%L  + ny*L,  // izquierda
		nx + ((ny-1+L)%L)*L, // abajo
		nx + ((ny+1+L)%L)*L  // arriba
	);
}

int test_utils(int L)
{
	int N=L*L;

	thrust::tuple<int,int,int,int> vecs;				

	for(int n=0;n<N;n++)
	{
		{
			std::cout << "vecinos de " << n << ":\n";
			vecs=vecinos2d(n,L);
			std::cout << thrust::get<0>(vecs) << " ";
			std::cout << thrust::get<1>(vecs) << " ";
			std::cout << thrust::get<2>(vecs) << " ";
			std::cout << thrust::get<3>(vecs) << "\n";
		}

	};

	std::cout << "\n";
	for(int nx=0;nx<L;nx++){
		for(int ny=0;ny<L;ny++){
			std::cout << nx+ny*L << "(" << esNegro(nx+ny*L,L) << ")" << "\t"; 
		}
		std::cout << std::endl;
	}

	if(L%2==0) std::cout << "warning: para usar checkerboard L debe ser par" << std::endl;
	return 0;
}
*/

__device__
float uniform(int n, int seed, int t)
{
		// keys and counters 
		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
    		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=n; 
    		c[1]=seed;
    		c[0]=t;
		r = philox(c, k); 
     		return (u01_closed_closed_32_53(r[0]));
}

struct ficha
{
	bool color;
	int L;
	ficha(bool _color, int _L):color(_color),L(_L){};

	__device__ __host__
	bool operator()(int n){
		return ((n%L+int(n/L))%2==color); // true si n es color
	}	
};

struct metropolis
{
	int L;
	float T;
	int *Mptr;
	int t;
	int seed;
	metropolis(int * _Mptr, float _T, int _L, int _t, int _seed):
	Mptr(_Mptr),T(_T),L(_L),t(_t),seed(_seed){};	

	__device__
	int operator()(int n){

		int nx=n%L;
	       	int ny=int(n/L);

		int local_field=
		Mptr[(nx-1+L)%L  + ny*L] + Mptr[(nx+1+L)%L  + ny*L] +
		Mptr[nx+((ny+1+L)%L)*L]  + Mptr[nx+((ny-1+L)%L)*L];

		// contribucion de nuestro spin sin flipear a la energia  
		float ene0=-Mptr[n]*local_field;	

		// contribucion a la energia de nuestro spin flipeado
		//int ene1=M[n]*vecinos;
		float ene1=Mptr[n]*local_field;	

		// metropolis: aceptar flipeo solo si r < exp(-(ene1-ene0)/temp)
		float p=exp(-(ene1-ene0)/T);

		// numero random entre [0,1] uniforme
		//float r=float(rand())/RAND_MAX;->philox
		float rn = uniform(n, seed, t);

		//if(rn<p) return -Mptr[n]; 
		//else return Mptr[n];		

		return (rn<p)?(-Mptr[n]):(Mptr[n]);
	}	
};


void print_campo_de_magnetizacion(thrust::device_vector<int> &M, int L)
{
	std::cout << "\n";
	for(int ny=0;ny<L;ny++){
		for(int nx=0;nx<L;nx++){
			std::cout << M[nx+ny*L] << "\t"; 
		}
		std::cout << std::endl;
	}
}


int main(int argc, char **argv)
{
	int globalseed=123456;

	int L=atoi(argv[1]);
	int nrun=atoi(argv[2]);
	float T=atof(argv[3]);


	int N=L*L;

	//test_utils(L);
	if(L%2==0) std::cout << "warning: para usar checkerboard L debe ser par" << std::endl;


	thrust::device_vector<int> M(N);
	int *Mraw=thrust::raw_pointer_cast(M.data());

	for(int n=0;n<N;n++) M[n]=(rand()*1.0/RAND_MAX>0.5)?(1):(-1);

	//print_campo_de_magnetizacion(M, L);
	for(int nt=0;nt<nrun;nt++)
	{
		std::cout << thrust::reduce(M.begin(),M.end())*1.0/N << std::endl;
		for(int color=0;color<2;color++){
			//std::cout << "Metropolis " << color << std::endl;
			thrust::transform_if(
				thrust::make_counting_iterator(0), thrust::make_counting_iterator(N), //rango
				M.begin(), // output
				metropolis(Mraw,T,L,nt,globalseed), // operacion 
				ficha(color, L) // predicado
			);
		}
	}
	//print_campo_de_magnetizacion(M, L);

	return 0;
}
