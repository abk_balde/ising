#include <thrust/execution_policy.h>
#include <thrust/functional.h>

///////////////////////////////////////////////////////////
ising_system *Sptr;
int paso;
float fieldmax, fieldmin;
int sqx, sqy;
int signo;
float hext;
float temp;
int planoz;
#ifndef DIM
#define DIM	256
#endif
#define HINC 0.01
#define TINC 0.01

std::ofstream outmag("magnetizacion.dat");

void rescale(){
  /*float *d = Sptr->dptr;
  //fieldmax = thrust::reduce(thrust::device, d, d+DIM*DIM, 0.f, thrust::maximum<float>());
  fieldmax = thrust::transform_reduce(thrust::device, d, d+DIM*DIM, absfloat(),0.f, thrust::maximum<float>());
  */
  fieldmax=1.f;		
  fieldmin=-1.f;		
}

// functor para mapear buffer de opengl al del sistema y viceversa (overloaded)
struct mapear_cuda_ogl_op
{
    float max, min;
    mapear_cuda_ogl_op(float _min, float _max):max(_max),min(_min){};

    __device__
    uchar4 operator()(int v)
    {
      uchar4 u4;
      u4.x = u4.y = u4.z = u4.w = 0;
      u4.y = (v>0)?(255):(0);
      u4.x = (v>0)?(255):(0);
      //u4.y = int((v-min)*255/max);
      return u4;
    }
    __device__
    float operator()(uchar4 u)
    {
      return (u.y*max/255.f)-(u.x*max/255.f);
    }
};

void mapear_cuda_ogl(uchar4 *ptr,int *d){
    //float min=-3; float max=3;
    //max = thrust::reduce(thrust::device, d, d+DIM*DIM, 0.f, thrust::maximum<float>());
    #ifdef DIMZ	
    thrust::transform(thrust::device, d+planoz*DIM*DIM, d+(planoz+1)*DIM*DIM, ptr, mapear_cuda_ogl_op(fieldmin,fieldmax));  
    #else
    thrust::transform(thrust::device, d, d+DIM*DIM, ptr, mapear_cuda_ogl_op(fieldmin,fieldmax));  
    #endif		
};
void mapear_ogl_cuda(int *d, uchar4 *ptr){
    float min=-3; float max=3;
    thrust::transform(thrust::device, ptr, ptr+DIM*DIM, d, mapear_cuda_ogl_op(min, max));  
};


// lo que pasa cada vez que opengl llama a draw
void change_pixels(){

  //std::cout << "cambiando pixels...";

  cudaGraphicsMapResources( 1, &resource, NULL ); 
  uchar4* devPtr; 
  size_t  size; 
  cudaGraphicsResourceGetMappedPointer( (void**)&devPtr, &size, resource); 

  Sptr->dynamics(paso);

  outmag << Sptr->get_total_time() << " " << Sptr->magnetizacion() << " " << Sptr->get_temp() << " " << Sptr->get_field() << std::endl;  

  mapear_cuda_ogl(devPtr,Sptr->dptr);
  cudaGraphicsUnmapResources( 1, &resource, NULL ); 

  //std::cout << "done" << std::endl;

};

void inicializar_variables_globales_sistema()
{
  std::cout << "inicializando variables del sistema..."; 
  paso=1;
  signo=1;
  rescale();
  hext=0.f;
  temp=3.0;
  planoz=0;			
  Sptr->set_temp(temp);	
  Sptr->set_field(hext);	
  std::cout << "done" << std::endl;
}
////////////////////////////////////////////////

