#include<iostream>
#include<cstdlib>
#include<thrust/host_vector.h>
#include <thrust/tuple.h>

bool blanconegro(int n, int L, int shift)
{
	return ((n+shift)%2==(n/L)%2);
}

bool blanconegro2(int n, int L, int shift)
{
	int nx=n%L;
	int ny=int(n/L);
	return ((nx+ny)%2==0);
}

int izq(int nx, int ny, int L)
{
	return (nx-1+L)%L  + ny*L;
}

int der(int nx, int ny, int L)
{
	return (nx+1)%L  + ny*L;
}

int arr(int nx, int ny, int L)
{
	return nx%L  + nx + ((ny+1)%L)*L;
}

int aba(int nx, int ny, int L)
{
	return nx + ((ny-1+L)%L)*L;
}

thrust::tuple<int,int,int,int> vecinos2d(int n, int L)
{
	int nx=n%L;
       	int ny=int(n/L);
	return thrust::make_tuple(izq(nx,ny,L),der(nx,ny,L),arr(nx,ny,L),aba(nx,ny,L));
}


int main(int argc, char **argv)
{
	int shift = (argc>1)?(atoi(argv[1])%2):(0);
	int L=(argc>2)?(atoi(argv[2])):(4);
	int N=L*L;

	thrust::tuple<int,int,int,int> vecs;				

	for(int n=0;n<N;n++)
	{
		{
			std::cout << n << ":\n";
			vecs=vecinos2d(n,L);
			for(int nv=0;nv<4;nv++) std::cout << thrust::get<const nv>(vecs) << std::endl;
		}

	};
}
