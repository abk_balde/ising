#include<iostream>
#include<fstream>
#include<cmath>
#include<cstdlib>

int * alocar_matriz(int *M,int N)
{
	#ifndef SOLUCION
	return (int *)malloc(sizeof(int)*N);
	#else
	thrust::device_vector<int> Mvec(N);
	return (int *)thrust::raw_pointer_cast(Mvec.data());
	#endif	
}

// si shift=0(1) retorna true si es blanco(negro)
bool blanconegro(int n, int L, int shift)
{
	return ((n+shift)%2==(n/L)%2);
}
// si shift=0(1) retorna true si es blanco(negro)
bool blanconegro2(int n, int L, int shift)
{
	//int nx=n%L;
	//int ny=int(n/L);
	//return ((nx+ny)%2==shift);
	return ((n%L+int(n/L))%2==shift);
}

#define BLANCO	0
#define NEGRO	1
void metropolis(int *M, float temp, int L, int shift)
{
	int N=L*L;
	for(int n=0;n<N;n++)
	{
	if(blanconegro2(n,L,shift))
	{

		int nx=n%L;
		int ny=int(n/L);

		// los indices de los sitios vecinos
		// con condiciones de contorno periodicas
		int izq = ((nx-1+L)%L) + ny*L ;
		int der = ((nx+1)%L) + ny*L ;
		int aba = nx + ((ny+1)%L)*L ;
		int arr = nx + ((ny-1+L)%L)*L ;

		// la magnetizacion total de los vecinos
		int vecinos=M[izq]+M[der]+M[arr]+M[aba];
		
		// contribucion de nuestro spin sin flipear a la energia  
		int ene0=-M[n]*vecinos;	

		// contribucion a la energia de nuestro spin flipeado
		int ene1=M[n]*vecinos;	

		// metropolis: aceptar flipeo solo si r < exp(-(ene1-ene0)/temp)
		float p=exp(-(ene1-ene0)/temp);
		float r=float(rand())/RAND_MAX;
		if(r<p) M[n]*=-1;
	}}
}


// para imprimir una matriz LxL guardada en el HOST
void print_matrix(int *M, std::ofstream &fout, int L)
{
	for(int i=0;i<L;i++){ 
		for(int j=0;j<L;j++){ 
			fout << M[i*L+j] << " "; 
		}
		fout << "\n";
	}
	fout << "\n" << std::endl;
}

// para imprimir una matriz LxL guardada en el HOST
void print_matrix(int *A, int *B, std::ofstream &fout, int L)
{
	for(int ny=0;ny<L;ny++){ 
		for(int nx=0;nx<L;nx++){
			// los indices de los sitios vecinos
			// con condiciones de contorno periodicas
			int cen = nx+ny*L;
			int izq = ((nx-1+L)%L) + ny*L ;
			int der = ((nx+1)%L) + ny*L ;
			int aba = nx + ((ny+1)%L)*L ;
			int arr = nx + ((ny-1+L)%L)*L ;
 
			//if(A[cen]==A[izq] && A[cen]==A[der] && A[cen]==A[aba] && A[cen]==A[arr])
			if(A[cen]==A[izq] && A[cen]==A[aba])
			fout << A[ny*L+nx] << " "; 
			else fout << -10 << " " ;
		}
		fout << "\n";
	}
	fout << "\n" << std::endl;
}

// inicializa la matriz
void inicializar_matriz_random(int *M, int N)
{
	float r;
	for(int i=0;i<N;i++) 
	{
		r = float(rand())/RAND_MAX;
		M[i]=(r>0.5)?(1):(-1);
	}
}

// inicializa la matriz
void inicializar_matriz_magnetizado(int *M, int N)
{
	for(int i=0;i<N;i++) 
	{
		M[i]=1;
	}
}

// inicializa la matriz
void inicializar_matriz_pared(int *M, int N)
{
	for(int i=0;i<N;i++) 
	{
		M[i]=(i>N/2)?(1):(-1);
	}
}



// retorna la magnatizacion
float get_magnetizacion(int *M, int N)
{
	float sum=0.0;
	for(int i=0;i<N;i++) sum+=M[i];
	return sum*=1.0/N;
}





