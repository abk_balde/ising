// COMPILAR ASI 
// nvcc ising.cu -DDIMZ -arch=sm_20 -o 3d.out
// nvcc ising.cu -arch=sm_20 -o 2d.out

#include "util_GPU.h"

int main(int argc, char **argv)
{
	int args=1;
	// tamanio lateral del sistema (default=128)
	int L=(argc>args)?(atoi(argv[args])):(128);
	if(L%2==1 || L<0){
		std::cout << "error: L debe ser par positivo" << std::endl;
		exit(1);	
	}
	args++;
	
	#ifdef DIMZ
	int Lz = (argc>args)?(atoi(argv[args])):(2);
	if(Lz%2==1 || Lz<0){
		std::cout << "error: Lz debe ser par positivo" << std::endl;
		exit(1);	
	}	
	args++;
	#endif

	// temperatura (default=2.0)
	float temp=(argc>args)?(atof(argv[args])):(2.0);
	args++;

	// campo (default=0.0)
	float field=(argc>args)?(atof(argv[args])):(0.0);
	args++;

	// iteraciones totales (default=100)
	int trun=(argc>args)?(atoi(argv[args])):(1000);
	args++;

	// semilla global (default=0)
	int seed = (argc>args)?(atoi(argv[args])):(0);
	srand(seed);
	//args++;

	std::cout << "L="     << L    << std::endl;
	#ifdef DIMZ
	std::cout << "Lz="     << Lz  << std::endl;
	#endif
	std::cout << "temp="  << temp << std::endl;
	std::cout << "field=" << field << std::endl;
	std::cout << "trun="  << trun << std::endl;
	std::cout << "seed="  << seed << std::endl;


/////// DESDE AQUI HACER TODO EN DEVICE ///////

	/*thrust::host_vector<int> M_h(N,1);
	thrust::device_vector<int> M(M_h);
	int *M_ptr=thrust::raw_pointer_cast(&M[0]);
	*/

	#ifndef DIMZ
	ising_system S(L, temp, field, seed);
	#else
	ising_system S(L, Lz, temp, field, seed);
	#endif

	std::ofstream magnetizacion_out("magnetizacion.dat");

	#ifdef MOVIE
	std::ofstream evolucion_out("evolucion.dat");
	#endif

	// loop temporal
	for(int t=0;t<trun;t++){

		magnetizacion_out << S.magnetizacion() << std::endl;
		S.MC_step();

		// compile con nvcc -DMOVIE=numero para activar
		// donde numero define la separacion entre cuadros
		// para hacer una pelicula (ver readme.txt)
		// use con cuidado, no imprima archivos gigantescos...
		// note que el ultimo frame siempre se imprime
		#ifdef MOVIE
		if(t%MOVIE==0 || t==trun-1){
			S.print_frame(evolucion_out);
		}
		#endif
	}

////////////////////////////////////////////////

	return 0;
}

// g++ -O2 -x c++ ising.cu -fopenmp -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP -lgomp -I /usr/local/cuda-5.5/include/
