#include<iostream>
#include<fstream>
#include<cmath>
#include<cstdio>
#include<cstdlib>
#include<thrust/device_vector.h>
#include<thrust/host_vector.h>
#include<thrust/reduce.h>
#include<thrust/for_each.h>
#include<thrust/copy.h>
#include <thrust/fill.h>
#include <thrust/tuple.h>
#include<thrust/iterator/counting_iterator.h>
#include <thrust/iterator/transform_iterator.h>
#include <thrust/iterator/permutation_iterator.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/functional.h>

/* counter-based random numbers */
// http://www.thesalmons.org/john/random123/releases/1.06/docs/
#include <Random123/philox.h> // philox headers
#include <Random123/u01.h>    // to get uniform deviates [0,1]
typedef r123::Philox2x32 RNG; // particular counter-based RNG

#define PARES	0
#define IMPARES	1
#define BLANCO	0
#define NEGRO	1

////////////////////////////////////////////////////////
// FUNCIONES DE INDEXADO
// DIMZ 
unsigned int linearIndexFromCoordinate(unsigned x, unsigned y, unsigned z, unsigned max_x, unsigned
 max_y)
{
    unsigned b = max_x ;
    unsigned c = (max_x ) * (max_y );
    return x + b*y + c*z;
}
thrust::tuple<unsigned,unsigned,unsigned> 
coordinateFromLinearIndex(unsigned idx, unsigned max_x, unsigned max_y)
{
    unsigned x =  idx % (max_x);
    idx /= (max_x);
    unsigned y = idx % (max_y);
    idx /= (max_y);
    unsigned z = idx;
    return thrust::tuple<unsigned,unsigned,unsigned>(x,y,z);
}
// 2D 
unsigned int linearIndexFromCoordinate(unsigned x, unsigned y, unsigned max_x)
{
    unsigned b = max_x ;
    return x + b*y;
}
thrust::tuple<unsigned,unsigned> 
coordinateFromLinearIndex(unsigned idx, unsigned max_x)
{
    unsigned x =  idx % (max_x);
    idx /= (max_x);
    unsigned y = idx;
    return thrust::tuple<unsigned,unsigned>(x,y);
}
////////////////////////////////////////////////////////////////////////



__device__
float uniform(int n, int seed, int t)
{
		// keys and counters 
		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
    		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=n; 
    		c[1]=seed;
    		c[0]=t;
		r = philox(c, k); 
     		return (u01_closed_closed_32_53(r[0]));
}

#ifdef RBDISORDER
__device__
float random_bond(int n1, int n2, int seed)
{
		// keys and counters 
		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
    		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=seed; 
    		c[0]=(n1>n2)?(n1):(n2);
    		c[1]=(n1>n2)?(n2):(n1);
		r = philox(c, k); 
     		return (u01_closed_closed_32_53(r[0])-0.5)*RBDISORDER;
}
#endif

#ifdef RFDISORDER
__device__
float random_field(int n1, int seed)
{
		// keys and counters 
		RNG philox; 	
    		RNG::ctr_type c={{}};
    		RNG::key_type k={{}};
    		RNG::ctr_type r;
    		// Garantiza una secuencia random "unica" para cada thread	
    		k[0]=seed; 
    		c[0]=0;
    		c[1]=n1;
		r = philox(c, k); 
     		return (u01_closed_closed_32_53(r[0])-0.5)*RFDISORDER;
}
#endif

#ifndef JY
#define JY	1.0
#endif

struct metropolitor
{
	int *M;
	float temp;
	float field;
	int L, Lz;
	int shift;
	int N;
	int t;
	int seed;
	float Jizq, Jder, Jarr, Jaba, Jade, Jatr;

	#ifndef DIMZ
	metropolitor(int *_M, float _temp, float _field, int _L, int _shift, int _t, int _seed):
	M(_M),temp(_temp),field(_field),L(_L),shift(_shift),t(_t), seed(_seed)
	{
		N=L*L;
		Jizq=Jder=Jarr=Jaba=Jade=Jatr=1.0;
		Jarr=Jarr*JY;
		Jaba=Jaba*JY;
	};	
	#else
	metropolitor(int *_M, float _temp, float _field, int _L, int _Lz, int _shift, int _t, int _seed):
	M(_M),temp(_temp),field(_field),L(_L),Lz(_Lz),shift(_shift),t(_t), seed(_seed)
	{
		N=L*L*Lz;
		Jizq=Jder=Jarr=Jaba=Jade=Jatr=1.0;		
	};	
	#endif



	__device__
	void operator()(int i)
	{


		#ifdef DIMZ
		int n = (2*i) + (int((2*i)/L)+int((2*i)/(L*L))+shift)%2;
		#else
		int n = (2*i) + (int((2*i)/L)+shift)%2;
		#endif
		
		//int n = 2*i + (int((2.0*i)/L)%2)*(1-2*shift) + shift;
		//printf("%d ",n);		
		
		// con condiciones de contorno periodicas en y pero helicas con paso 1 en X...
		int nx = (n%L);
		int ny = int(n/L)%L; 
		#ifdef DIMZ
		int nz = int(n/(L*L));
		#endif	
		//thrust::tuple<unsigned,unsigned> t = coordinateFromLinearIndex(n, L);
		//int nx=thrust::get<0>(t); int ny=thrust::get<1>(t);

		#ifndef DIMZ
		int izq=(nx-1+L)%L+ny*L;
		int der=(nx+1)%L+ny*L;
		int arr=nx+((ny-1+L)%L)*L;
		int aba=nx+((ny+1)%L)*L;
		#else
		int izq= (nx-1+L)%L + ny*L           + nz*L*L;
		int der= (nx+1)%L   + ny*L           + nz*L*L;
		int arr= nx         + ((ny-1+L)%L)*L + nz*L*L;
		int aba= nx         + ((ny+1)%L)*L   + nz*L*L;
		int ade= nx         + ny*L           + ((nz+1)%Lz)*L*L;
		int atr= nx         + ny*L           + ((nz-1+Lz)%Lz)*L*L;;
		#endif


		#ifdef RBDISORDER
		Jizq+=random_bond(n,izq,seed);
		Jder+=random_bond(n,der,seed);
		Jarr+=random_bond(n,arr,seed);
		Jaba+=random_bond(n,aba,seed);
		#ifdef DIMZ
		Jade+=random_bond(n,ade,seed);
		Jatr+=random_bond(n,atr,seed);
		#endif
		#endif

		// la magnetizacion total de los vecinos
		#ifndef DIMZ
		int vecinos=Jizq*M[izq]+Jder*M[der]+Jarr*M[arr]+Jaba*M[aba];
		#else
		int vecinos=Jizq*M[izq]+Jder*M[der]+Jarr*M[arr]+Jaba*M[aba]+Jade*M[ade]+Jatr*M[atr];
		#endif

		#ifdef RFDISORDER
		field+=random_field(n, seed);
		#endif

		// contribucion de nuestro spin sin flipear a la energia  
		float ene0=-M[n]*(vecinos+field);	

		// contribucion a la energia de nuestro spin flipeado
		//int ene1=M[n]*vecinos;
		float ene1=M[n]*(vecinos+field);;	

		// metropolis: aceptar flipeo solo si r < exp(-(ene1-ene0)/temp)
		float p=exp(-(ene1-ene0)/temp);

		// numero random entre [0,1] uniforme
		//float r=float(rand())/RAND_MAX;->philox
		float rn = uniform(n, seed, t);

		if(rn<p) M[n]*=-1;	
	}
};


// para imprimir una matriz LxL guardada en el HOST
#ifndef DIMZ
void print_matrix(int *M, std::ofstream &fout, int L)
#else
void print_matrix(int *M, std::ofstream &fout, int L, int Lz)
#endif
{
	#ifndef DIMZ
	for(int i=0;i<L;i++){ 
		for(int j=0;j<L;j++){ 
			fout << M[i*L+j] << " "; 
		}
		fout << "\n";
	}
	fout << "\n" << std::endl;
	#else
	for(int k=0;k<Lz;k++){ 
	for(int j=0;j<L;j++){ 
	for(int i=0;i<L;i++){ 
			fout << M[k*L*L+j*L+i] << " "; 
	}
	fout << "\n";
	}
	fout << "\n";
	}
	fout << "\n" << std::endl;
	#endif
}


class ising_system
{

	public:
        void init(){
		#ifndef DIMZ
		N=L*L; //2d
		#else
		N=L*L*Lz; //3d
		#endif

		M_h.resize(N);
		thrust::fill(M_h.begin(),M_h.end(),1);

		M.resize(N);
		thrust::copy(M_h.begin(),M_h.end(),M.begin());

		M_ptr=thrust::raw_pointer_cast(&M[0]);
		dptr=M_ptr;
	
		tiempo=0;

		std::ofstream log("logfile.dat");
		log << "L=" << L << std::endl;
		#ifdef DIMZ
		log << "Lz=" << Lz << std::endl;
		#endif
		log << "temp=" << temp << std::endl;
		log << "field=" << field << std::endl;
		log << "seed=" << seed << std::endl;
	}
  
	//2D
	ising_system(int _L):L(_L),temp(3.f),seed(12345),field(0.f)
	{
		Lz=0;
		init();
	}
	ising_system(int _L, float _temp, float _field, int _seed):L(_L),temp(_temp),seed(_seed),field(_field)
	{
		Lz=0;
		init();
	}

	//3D
	ising_system(int _L, int _Lz):L(_L),Lz(_Lz),temp(3.f),seed(12345),field(0.f)
	{
		init();
	}	
	ising_system(int _L, int _Lz, float _temp, float _field, int _seed):L(_L),Lz(_Lz),temp(_temp),seed(_seed),field(_field)
	{
		init();
	}


	void reinicializar(int signo)
	{
		tiempo=0;
		thrust::fill(M.begin(),M.end(),signo);
	}
	
	// funciona solo en 2D como esta...
	void addsquare(int x, int y, int z, int w, int signo)
	{
		#ifndef DIMZ
		//std::cout << "adding square" << std::endl;
		int ww=(w>L)?(L):(w);
		for(int j=y-ww/2;j<y+ww/2;j++){
		for(int i=x-ww/2;i<x+ww/2;i++){
			M[(i+L)%L+((j+L)%L)*L]=signo;
		}}
		#else
		//std::cout << "adding square" << std::endl;
		int ww=(w>L)?(L):(w);
		//int wwz=(w>Lz)?(Lz):(w);
		//for(int k=z-wwz/2;k<z+wwz/2;k++){
		for(int j=y-ww/2;j<y+ww/2;j++){
		for(int i=x-ww/2;i<x+ww/2;i++){
			M[(i+L)%L+((j+L)%L)*L+z*L*L]=signo;
		}}
		//}
		#endif
	}


	// one checkerboar MC step
	void MC_step(){
		int t=tiempo;
		int npares=N/2;
		int nimpares=N/2;

		#ifndef DIMZ	
		metropolitor oppar(M_ptr,temp,field,L,PARES,t,seed);
		metropolitor opimp(M_ptr,temp,field,L,IMPARES,t,seed);
		#else
		metropolitor oppar(M_ptr,temp,field,L,Lz,PARES,t,seed);
		metropolitor opimp(M_ptr,temp,field,L,Lz,IMPARES,t,seed);
		#endif

		thrust::for_each(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(npares),
			oppar
		);	

		thrust::for_each(
			thrust::make_counting_iterator(0),thrust::make_counting_iterator(nimpares),
			opimp
		);	
		tiempo++;
	}

	void dynamics(int trun)
	{
		for(int t=0;t<trun;t++){
			MC_step();
		}
	}

	void print_frame(std::ofstream &evolucion_out){
		thrust::copy(M.begin(),M.end(),M_h.begin());
		#ifndef DIMZ
		print_matrix(M_h.data(),evolucion_out,L);
		#else
		print_matrix(M_h.data(),evolucion_out,L,Lz);
		#endif
	}

	float magnetizacion(){
		return float(thrust::reduce(M.begin(),M.end()))/N;
	}

	void set_temp(float newtemp){
		temp=newtemp;
	}

	void set_field(float newfield){
		field=newfield;
	}

	float get_temp(){
		return temp;
	}

	float get_field(){
		return field;
	}

	int get_total_time(){
		return tiempo;
	}

	int *dptr;

	private:
	thrust::host_vector<int> M_h;
	thrust::device_vector<int> M;

	int *M_ptr;
	int N;
	int L;
	int Lz;	
	float temp;
	float field;
	int seed;
	int tiempo;
};



